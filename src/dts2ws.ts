// This code is licenced under the GNU GPLv3
// (c) David Koňařík 2021
import ts from "typescript";
import arg from "arg";
import { assert } from "console";

type Type =
    | SimpleType
    | GenericType
    | OptionalType
    | UnionType
    | FunctionType

interface SimpleType {
	kind: "simple";
    name: string;
}

interface GenericType {
	kind: "generic";
    name: string;
    params: Type[];
}

interface OptionalType {
	kind: "optional";
    inner: Type;
}

interface UnionType {
	kind: "union";
    inners: Type[];
}

interface Param {
	name: string;
	type: Type;
}

interface FunctionType {
	kind: "function";
    params: Param[];
    returnType: Type;
}

interface Method {
    kind: "method";
    name: string;
    returnType: Type;
    params: Param[];
}

interface Property {
    kind: "property";
    name: string;
    type: Type;
}

interface Constructor {
    kind: "constructor";
    params: Param[];
}

type Construct =
    | Class
    | Interface
    | TypeAlias
    | FreeFunctions

interface Class {
    kind: "class";
    nsPath: string[];
    members: (Method | Property | Constructor)[];
    extended?: Type;
    implemented: Type[];
}

interface Interface {
    kind: "interface";
    nsPath: string[];
    members: (Method | Property)[];
    implemented: Type[];
}

interface TypeAlias {
    kind: "alias";
    nsPath: string[];
    type: Type;
}

interface Function {
    name: string;
    returnType: Type;
    params: {name: string, type: Type}[];
}

interface FreeFunctions {
    kind: "freeFuncs";
    nsPath: string[];
    functions: Function[];
}

function stringyUniq<T>(items: T[]): T[] {
    const set = new Set(items.map(x => JSON.stringify(x)));
    return Array.from(set).map(s => JSON.parse(s));
}

function makeOptional(t: Type): Type {
	if(t.kind == "optional") {
		return t;
	}
	return {kind: "optional", inner: t};
}

function processType(tc: ts.TypeChecker, t: ts.Type): Type {
    const tr = <ts.TypeReference>t;
    // TODO: How to get names of some types without this ugly cast?
    const tAny = <any>t;

    function getName() {
        const sym = t.getSymbol();
        if(sym) {
            return tc.getFullyQualifiedName(sym);
        } else {
            return tAny.intrinsicName || "";
        }
    }

    // TODO: Support unions of literals as enums
    if(t.isNumberLiteral()) {
        return { kind: "simple", name: "number" };
    } else if(t.isStringLiteral()) {
        return { kind: "simple", name: "string" };
    } else if(t.flags & ts.TypeFlags.BooleanLike) {
        return { kind: "simple", name: "boolean" }
    // Bools are internally unions in TypeScript, neat
    } else if(t.isUnion() && tAny.intrinsicName != "boolean") {
        const inners = stringyUniq(t.types.map(t => processType(tc, t)));
        // This is a roundabout way to delete duplicates
        if(inners.length == 1) {
            return inners[0];
        } else {
            return { kind: "union", inners: inners };
        }
    } else if(tr.typeArguments && tr.typeArguments.length > 0) {
        return  {
            kind: "generic",
            name: getName(),
            params: tr.typeArguments?.map(ta => processType(tc, ta)) || [],
        };
    } else {
        return  {
            kind: "simple",
            name: getName(),
        };
    }
}


// Returns main type of the node and an array of created aux types (i.e.
// interfaces for inline types, which wouldn't work in WIG)
function processTypeNode(
		tc: ts.TypeChecker, n: ts.TypeNode | undefined, nsPath: string[]): [Type, Construct[]] {
    if(!n) {
        console.warn("Type is missing, presuming void");
        return [{
            kind: "simple",
            name: "void",
        }, []];
    }

    // It seems you can't get function type data from ts.Type, only from
    // ts.TypeNode, which is why we need all this...
    if(ts.isParenthesizedTypeNode(n)) {
        return processTypeNode(tc, n.type, nsPath);
    } else if(ts.isArrayTypeNode(n)) {
		const [paramType, aux] = processTypeNode(tc, n.elementType, nsPath);
        return [{
            kind: "generic",
            name: "Array",
            params: [paramType],
        }, aux];
    } else if(ts.isUnionTypeNode(n)) {
		const allAux: Construct[] = [];
        const inners = stringyUniq(n.types.map((tn, i) => {
			const [t, aux] = processTypeNode(tc, tn, [...nsPath, `opt${i}`]);
			allAux.push(...aux);
			return t;
		}));
        return [{ kind: "union", inners }, allAux];
    } else if(ts.isFunctionTypeNode(n)) {
		const [retType, retAux] = processTypeNode(tc, n.type, [...nsPath, "ret"]);
		const paramAux: Construct[] = [];
        return [{
            kind: "function",
            params: n.parameters.map(p => {
				const [t, aux] = processTypeNode(tc, p.type, [...nsPath, p.name.getText()]);
				paramAux.push(...aux);
				return {
					name: p.name.getText(),
					type: t,
				};
            }),
            returnType: retType,
        }, [...retAux, ...paramAux]];
    } else if(ts.isTypeLiteralNode(n)) { // Anonymous object
		const innerAux: Construct[] = [];
		return [{
			kind: "simple",
			name: nsPath.join("."),
		}, [{
			kind: "interface",
			nsPath,
			members: <Property[]> n.members
				.map(m => {
					if(!ts.isPropertySignature(m)) {
						console.warn("Non-PropertySignature node in inline object type:", nsPath.join("."));
						return null;
					}
					const [type, aux] = processTypeOfNode(tc, m, [...nsPath, m.name.getText()]);
					innerAux.push(...aux);
					return {
						kind: "property",
						name: m.name.getText(),
						type,
					};
				})
				.filter(m => m !== null),
			implemented: [],
		}, ...innerAux]];
	}

    return [processType(tc, tc.getTypeFromTypeNode(n)), []];
}

function processTypeOfNode(
        tc: ts.TypeChecker,
        n: {questionToken?: ts.QuestionToken, type?: ts.TypeNode},
		nsPath: string[]): [Type, Construct[]] {
    let [type, aux] = processTypeNode(tc, n.type, nsPath);
    if(n.questionToken) {
        type = makeOptional(type);
    }
    return [type, aux];
}

function processParam(tc: ts.TypeChecker, p: ts.ParameterDeclaration, nsPath: string[])
		: [Param, Construct[]] {
	const [type, aux] = processTypeOfNode(tc, p, [...nsPath, p.name.getText()]);
    return [{
        name: p.name.getText(),
        type,
    }, aux];
}

function visitClassOrInterface(
        tc: ts.TypeChecker, cls: ts.ClassDeclaration | ts.InterfaceDeclaration,
        nsPath: string[]): [Class | Interface | TypeAlias, Construct[]] {
	const aux: Construct[] = [];
    const members: (Method | Property | Constructor)[] = [];
    let extended: Type | undefined;
    const implemented: Type[] = [];
	const thisNsPath = [...nsPath, cls.name!.getText()];

	if(ts.isInterfaceDeclaration(cls)) {
		let csd: ts.CallSignatureDeclaration | undefined;
		cls.forEachChild(n => {
			if(n.kind == ts.SyntaxKind.CallSignature) {
				csd = <ts.CallSignatureDeclaration>n;
			}
		})
		if(csd != undefined) {
			// This interface is actually a function type alias
			const [returnType, rtAux] = processTypeOfNode(tc, csd, thisNsPath);
			aux.push(...rtAux);
			return [{
				kind: "alias",
				nsPath: [...nsPath, cls.name!.getText()],
				type: {
					kind: "function",
					params: csd.parameters.map(p => {
						const [t, paux] = processParam(tc, p, thisNsPath);
						aux.push(...paux);
						return t;
					}),
					returnType,
				},
			}, aux];
		}
	}

    cls.forEachChild(n => {
        if(ts.isMethodDeclaration(n) || ts.isMethodSignature(n)) {
			const methodNsPath = [...thisNsPath, n.name.getText()];
			const [returnType, rtAux] = processTypeOfNode(tc, n, [...methodNsPath, "ret"]);
			aux.push(...rtAux);
            members.push({
                kind: "method",
                name: n.name.getText(),
                returnType,
                params: n.parameters.map(p => {
					const [t, paux] = processParam(tc, p, methodNsPath);
					aux.push(...paux);
					return t;
				}),
            });
        } else if(ts.isPropertyDeclaration(n) || ts.isPropertySignature(n)) {
            let [type, taux] = processTypeOfNode(tc, n, [...thisNsPath, n.name.getText()]);
			aux.push(...taux);
            if(n.questionToken) {
                type = makeOptional(type);
            }
            members.push({
                kind: "property",
                name: n.name.getText(),
                type: type,
            });
        } else if(ts.isConstructorDeclaration(n)) {
			const ctorNsPath = [...thisNsPath, "ctor"];
            members.push({
                kind: "constructor",
                params: n.parameters.map(p => {
					const [t, paux] = processParam(tc, p, ctorNsPath);
					aux.push(...paux);
					return t;
				}),
            });
        } else if(ts.isHeritageClause(n)) {
            if(n.token == ts.SyntaxKind.ImplementsKeyword || ts.isInterfaceDeclaration(cls)) {
				if(ts.isInterfaceDeclaration(cls) && n.token == ts.SyntaxKind.ImplementsKeyword) {
					console.error("Heritage clause of interface not Extends!", cls.name.getText());
					return;
				}
                for(const t of n.types) {
                    implemented.push(
                        processType(tc, tc.getTypeAtLocation(t.expression)));
                }
			} else  {
                extended =
                    processType(tc,
                        tc.getTypeAtLocation(n.types[0].expression));
            }
		}
    });

	if(ts.isClassDeclaration(cls)) {
		return [{
			kind:  "class",
			nsPath: [...nsPath, cls.name!.getText()],
			members: members,
			extended, implemented,
		}, aux];
	} else {
		return [{
			kind:  "interface",
			nsPath: [...nsPath, cls.name!.getText()],
			members: <(Method | Property)[]> members,
			implemented,
		}, aux];
	}
}

function visitFunction(
        tc: ts.TypeChecker, fun: ts.FunctionDeclaration,
        nsPath: string[]): [Function, Construct[]] {
	const thisNsPath = [...nsPath, fun.name!.getText()];
	const [returnType, aux] = processTypeOfNode(tc, fun, [...thisNsPath, "ret"]);
    return [{
        name: fun.name?.getText() || "",
        params: fun.parameters.map(p => {
			const [t, paux] = processParam(tc, p, thisNsPath);
			aux.push(...paux);
			return t;
		}),
        returnType,
    }, aux];
}

function visitTopLevel(tc: ts.TypeChecker, node: ts.Node, nsPath: string[]) {
    const constructs: Construct[] = [];
    if(ts.isModuleDeclaration(node)) {
        const modName = node.name.getText();
        node.forEachChild(n => {
            const cs = visitTopLevel(tc, n, [...nsPath, modName]);
            constructs.push(...cs);
        });
    } else if(ts.isModuleBlock(node)) {
        const functions: Function[] = [];
        node.forEachChild(n => {
            const cs = visitTopLevel(tc, n, nsPath);
            constructs.push(...cs);

            if(ts.isFunctionDeclaration(n)) {
				const [fun, funAux] = visitFunction(tc, n, nsPath);
                functions.push(fun);
				constructs.push(...funAux);
            }
        });

        if(functions.length > 0) {
            constructs.push({kind: "freeFuncs", nsPath, functions});
        }
    } else if(ts.isClassDeclaration(node) || ts.isInterfaceDeclaration(node)) {
		const [cls, aux] = visitClassOrInterface(tc, node, nsPath);
        constructs.push(cls, ...aux);
    }
    return constructs;
}

function wigEscapeIdent(ident: string) {
    const keywords = [
        "abstract", "and", "as", "assert", "base", "begin", "class",
        "default", "delegate", "do", "done", "downcast", "downto", "elif",
        "else", "end", "exception", "extern", "false", "finally", "fixed",
        "for", "fun", "function", "global", "if", "in", "inherit", "inline",
        "interface", "internal", "lazy", "let", "let!", "match", "match!",
        "member", "module", "mutable", "namespace", "new", "not", "null",
        "of", "open", "or", "override", "private", "public", "rec", "return",
        "return!", "select", "static", "struct", "then", "to", "true", "try",
        "type", "upcast", "use", "use!", "val", "void", "when", "while",
        "with", "yield", "yield!", "const", "asr", "land", "lor", "lsl",
        "lsr", "lxor", "mod", "sig", "atomic", "break", "checked",
        "component", "const", "constraint", "constructor", "continue",
        "eager", "event", "external", "functor", "include", "method",
        "mixin", "object", "parallel", "process", "protected", "pure",
        "sealed", "tailcall", "trait", "virtual", "volatile", "params"];
    if(keywords.includes(ident) || /[0-9]/.test(ident[0])) {
        return "``" + ident + "``";
    } else {
        return ident;
    }
}

function getWigName(nsPath: string[] | string, stripNss: string[]): string {
    if(nsPath instanceof Array) {
        return getWigName(nsPath.join("."), stripNss);
    }

    for(const ns of stripNss) {
        if(nsPath.startsWith(ns + ".")) {
            nsPath = nsPath.slice(ns.length + 1);
            break;
        }
    }

    return wigEscapeIdent(nsPath.replace(/\./g, "_"));
}

function wigType(
        t: Type, stripNss: string[], aliasMap: Map<string, Type>,
        selfName: string): string {
    if(t.kind == "simple") {
        const st = <SimpleType>t;
        if(st.name == "string") {
            return "T<string>";
        } else if(st.name == "number") {
            return "T<float>";
        } else if(st.name == "boolean") {
            return "T<bool>";
        } else if(st.name == "any" || st.name == "object"
                  || st.name == "Object") {
            return "T<obj>";
        } else if(st.name == "void") {
            return "T<unit>";
        } else if(st.name == "Function") {
            return "(T<unit> ^-> T<unit>)";
        } else if(st.name == "__type") {
            console.warn("Encountered undefined type!");
            return "T<obj>";
        // This is a non-exhaustive list of browser-standard types that should
        // translate to a WebSharper version
        } else if(/HTML.*Element/.test(st.name)) {
            return "T<HTMLElement>";
        } else if(st.name == "Date") {
            return "T<Date>";
        } else if(st.name == selfName) {
            return "TSelf";
        } else if(aliasMap.has(st.name)) {
            return wigType(
                aliasMap.get(st.name)!, stripNss, aliasMap, selfName);
        } else {
            return getWigName(st.name, stripNss);
        }
    } else if(t.kind == "generic") {
        const gt = <GenericType>t;
        if(gt.name == "Array") {
            return "(!| " + wigType(gt.params[0], stripNss,
                                    aliasMap, selfName) + ")";
        } else {
            console.error("Arbitrary generics aren't supported yet!");
            return "TODO-generic"
        }
    } else if(t.kind == "optional") {
        const ot = <OptionalType>t;
        return "(!? " + wigType(ot.inner, stripNss, aliasMap, selfName) + ")";
    } else if(t.kind == "union") {
        const ut = <UnionType>t;
        return "(" + ut.inners.map(i =>
            wigType(i, stripNss, aliasMap, selfName)).join(" + ") + ")";
    } else if(t.kind == "function") {
        const ft = <FunctionType>t;
        let params =
            ft.params.map(p =>
                wigType(p.type, stripNss, aliasMap, selfName)
                + "?" + wigEscapeIdent(p.name))
            .join(" * ");
        if(params == "") {
            params = "T<unit>";
        }
        return "(" + params + " ^-> "
            + wigType(ft.returnType, stripNss, aliasMap, selfName) + ")";
    }
    return "UNREACHABLE";
}

function wigMember(
        m: Constructor | Method | Property,
        stripNss: string[], typeAliasMap: Map<string, Type>,
        selfName: string) {
    if(m.kind == "constructor") {
        const ctor = <Constructor>m;
        const params = ctor.params.map(p => {
            const type = wigType(p.type, stripNss, typeAliasMap, selfName);
            const name = wigEscapeIdent(p.name);
            return `${type}?${name}`
        }).join(" * ");
        return `Constructor (${params})`;
    } else if(m.kind == "method") {
        const method = <Method>m;
        let params = method.params.map(p => {
            const type = wigType(p.type, stripNss, typeAliasMap, selfName);
            const name = wigEscapeIdent(p.name);
            return `${type}?${name}`;
        }).join(" * ");
        if(params == "") {
            params = "T<unit>";
        }
        const ret = wigType(
            method.returnType, stripNss, typeAliasMap, selfName);
        return `"${method.name}" => ${params} ^-> ${ret}`;
    } else if(m.kind == "property") {
        const prop = <Property>m;
        return `"${prop.name}" =@ ${wigType(prop.type, stripNss,
                                            typeAliasMap, selfName)}`;
    }
}

function getParentInterfaces(
        constructs: Construct[], iface: Interface): Interface[] {
    return (<Interface[]>[]).concat(...iface.implemented.map(parentRef => {
        assert(parentRef.kind == "simple");
        const parentSt = <SimpleType> parentRef;
        const parent = constructs.find(
            c => c.nsPath.join(".") == parentSt.name);
        if(!parent) {
            console.error(`Parent interface of ${iface.nsPath.join(".")} (${parentSt.name}) not found!`);
            return [];
        }
        assert(parent.kind == "interface");
        const parentIface = <Interface> parent;
        return [parentIface, ...getParentInterfaces(constructs, parentIface)];
    }));
}

function createWig(
        constructs: Construct[], stripNss: string[]) {
    let out = "";
    const freeFuncs =
        constructs
        .filter(c => c.kind == "freeFuncs")
        .map(c => <FreeFunctions>c);
    const classesAndIfaces =
        constructs
        .filter(c => c.kind == "class" || c.kind == "interface");
    // We can't "delay-initialise" these like the classes, so we'll have to just
    // expand them upon usage
    const typeAliasMap = new Map<string, Type>();
    constructs
    .filter(c => c.kind == "alias")
    .forEach(c => {
        const ta = <TypeAlias>c;
        typeAliasMap.set(ta.nsPath.join("."), ta.type);
    });

    out +=
`type Helpers =
    static member NormaliseType(t: Type.Type) = t
    static member NormaliseType(t: CodeModel.Class) = t.Type

`;

    // First forward-declare everything to allow out-of-order references
    for(const c of classesAndIfaces) {
        const wigName = getWigName(c.nsPath, stripNss);

        out += `let ${wigName} = Class "${c.nsPath.join(".")}"\n`;
    }
    out += "\n";

    for(const c of classesAndIfaces) {
        const name = c.nsPath[c.nsPath.length - 1];
        // TODO: Nested classes/namespaces?
        const wigName = getWigName(c.nsPath, stripNss);

        out += wigName + "\n";
        //out += `    |> WithSourceName "${c.nsPath.join(".")}"\n`;
        out += `    |> WithSourceName "${wigName}"\n`;

        let members;
        if(c.kind == "class") {
            const cls = <Class>c;
            members = cls.members;

            if(cls.extended) {
                const t = wigType(cls.extended, stripNss, typeAliasMap, name);
                out += `    |=> Inherits ${t}\n`;
            }
            if(cls.implemented.length > 0) {
                const types = cls.implemented
                    .map(t => wigType(t, stripNss, typeAliasMap, name))
                    .join("; ");
                out += `    |=> Implements [${types}]\n`;
            }
        } else {
            const iface = <Interface>c;

            const ancestry = [iface, ...getParentInterfaces(constructs, iface)];
            const allMembers = (<(Property | Method)[]>[]).concat(
                ...ancestry.map(p => p.members));
            let allProps = allMembers
                .filter(m => m.kind == "property")
                .map(p => <Property>p);
            // Deduplicate (why is this needed?)
            allProps = allProps.filter((val, idx, arr) =>
                arr.findIndex(p => p.name == val.name) == idx);

            // We would get collisions with the setter functions if we actually
            // did inheritance, so we don't formally inherit and flatten the
            // members. This has the potential to break a lot of things, but
            // it'll mostly work

            members = [...allProps, ...allMembers.filter(m => m.kind != "property")];

            /*if(iface.implemented.length > 0) {
                // We translate TS interfaces into WIG classes, so we can only
                // inherit one
                if(iface.implemented.length > 1) {
                    console.error(
                        "Interface", name,
                        "inherits multiple times, this won't work!");
                }
                const type = wigType(iface.implemented[0], stripNss, name);
                out += `    |=> Inherits ${type}\n`;
            }*/

            // Unfortunately, due to the fact that each type variant of a function
            // has to be compiled into a separate .NET method, we can't have
            // constructor methods taking too many arguments that are erased unions.
            // (exponential growth). Instead, we'll create chainable Set... methods
            // (TODO: Colisions?) for all types and only create constructors for
            // some.
            // In the current implementation, optional params create multtiple
            // variants as well for some reason instead of default args :(

            const allOptProps = allProps.filter(p => p.type.kind == "optional");
            // TODO: Only count optional/union typed props
            if(allProps.length < 5 && allProps.length > 0
               && allOptProps.length > 0) {
                const fields =
                    allProps
                    .map(p => {
                        const t = wigType(p.type, stripNss, typeAliasMap, "");
                        const n = wigEscapeIdent(p.name);
                        return `${t}?${n}`;
                    })
                    .join(" * ");

                out += `    |+> Static [ ObjectConstructor (${fields}) ]\n`;
            } else {
                out += `    |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]\n`;
                out += "    |+> Instance [\n";
                for(const prop of allProps) {
                    const pName = prop.name[0].toUpperCase() + prop.name.slice(1);
					let type = prop.type;
					// Inlines + optional params seem to be broken
					while(type.kind == "optional") {
						type = type.inner;
					}
                    const pType = wigType(type, stripNss, typeAliasMap, name);
                    out += `        "Set${pName}" => ${pType}?v ^-> TSelf\n`;
                    out += `        |> WithInteropInline (fun tr -> "($this.${prop.name} = " + tr "v" + ", $this)")\n`;
                }
                out += "    ]\n";
            }
        }

        function propToPair(p: Property) {
            let t = wigType(p.type, stripNss, typeAliasMap, name);
            return `"${p.name}", Helpers.NormaliseType(${t})`;
        }

        const props = members
            .filter(m => m.kind == "property")
            .map(p => <Property> p);
        const optProps = props
            .filter(p => p.type.kind == "optional")
        const optPropsStr = optProps
            .map(p => {
                const t = wigType(p.type, stripNss, typeAliasMap, name);
                return `        "${p.name}" =@ ${t}\n`;
            })
            .join("");
        const nonOptProps = props
            .filter(p => p.type.kind != "optional");
        const nonOptPropsStr = nonOptProps
            .map(p => "        " + propToPair(p) + "\n")
            .join("");
        const otherMembers = members
            .filter(m => m.kind != "property")
            .map(m =>
                "        "
                + wigMember(m, stripNss, typeAliasMap, name) + "\n")
            .join("");

        if(otherMembers.length > 0) {
            out += `    |+> Instance [\n${otherMembers}    ]\n`;
        }
        if(nonOptProps.length > 0) {
            out += `    |+> Pattern.RequiredFields [\n${nonOptPropsStr}    ]\n`;
        }

        if(optProps.length > 0) {
            out += `    |+> Instance [\n${optPropsStr}    ]\n`;
        }

        out += "    |> ignore\n";
        out += "\n";
    }

    for(const ffuns of freeFuncs) {
        out += `let ${getWigName(ffuns.nsPath, stripNss)} =\n`;
        out += `    Class "${ffuns.nsPath.join(".")}"\n`;

        const funcs = ffuns.functions.map(f => {
            let params = f.params.map(p => {
                const t = wigType(p.type, stripNss, typeAliasMap, "");
                return `${t}?${p.name}`;
            }).join(" * ");
            if(params == "") {
                params = "T<unit>";
            }
            const ret = wigType(f.returnType, stripNss, typeAliasMap, "");
            return `        "${f.name}" => ${params} ^-> ${ret}\n`;
        });

        out += `    |+> Static [\n${funcs.join("")}    ]\n`;

    }

    out += "let NamespaceEntities: CodeModel.NamespaceEntity array = [|\n";
    for(const c of constructs) {
        if(c.kind == "alias") {
            continue;
        }
        const wigName = getWigName(c.nsPath, stripNss);
        out += `    ${wigName}\n`;
    }
    out += "|]\n";

    return out;
}

function processFiles(
        fileNames: string[],
        options: ts.CompilerOptions,
        stripNss: string[]): void {
    let program = ts.createProgram(fileNames, options);

    let tc = program.getTypeChecker();

    const constructs: Construct[] = [];
    for (const sourceFile of program.getSourceFiles()) {
        if(!fileNames.includes(sourceFile.fileName)) {
            continue;
        }
        if (!sourceFile.isDeclarationFile) {
            console.warn("Given non-declaration file, skipping...");
            continue;
        }
        ts.forEachChild(sourceFile, (n) => {
            const cs = visitTopLevel(tc, n, []);
            constructs.push(...cs);
        });
    }
    console.log(createWig(constructs, stripNss));
}

const args = arg({
    "--stripNamespace": [String],
});

processFiles(args._, {
        target: ts.ScriptTarget.ES5,
        module: ts.ModuleKind.CommonJS
    }, args["--stripNamespace"] || []);
